FROM node:10-alpine AS node

FROM node AS base
WORKDIR /opt/jupordown-api
RUN apk --no-cache --virtual build-dependencies add \
    git \
    python \
    make \
    g++ \
    openssh
COPY package*.json ./

FROM base AS dependencies
RUN npm --no-color --production --build-from-source i

FROM dependencies AS dev-dependencies
RUN npm --no-color ci

FROM dev-dependencies AS test
COPY . .

FROM dependencies AS release
COPY . .

FROM node AS final
ARG BUILD_NUMBER=1
WORKDIR /opt/jupordown-api
COPY --from=release /opt/jupordown-api ./
RUN npm --no-color --no-git-tag-version version $BUILD_NUMBER.0.0
EXPOSE 9000
CMD npm start
