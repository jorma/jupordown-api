const Router = require('express');
const { controller, validation } = require('../controllers/config');

const router = new Router();

router.get(
  '/:name',
  validation.get,
  controller.get
);

module.exports = router;
