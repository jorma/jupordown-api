const Router = require('express');
const passport = require('passport');
const { controller, validation } = require('../controllers/checks');

const router = new Router();

router.get(
  '/',
  validation.list,
  passport.authenticate('jwt', { session: false }),
  controller.list
);

router.post(
  '/',
  validation.create,
  passport.authenticate('jwt', { session: false }),
  controller.create
);

router.patch(
  '/:alias',
  validation.update,
  passport.authenticate('jwt', { session: false }),
  controller.update
);

router.get(
  '/detail',
  validation.detail,
  passport.authenticate('jwt', { session: false }),
  controller.detail
);

router.delete(
  '/:alias',
  validation.remove,
  passport.authenticate('jwt', { session: false }),
  controller.remove
);

module.exports = router;
