const express = require('express');
const authRouter = require('./auth');
const checkRouter = require('./checks');
const configRouter = require('./config');

const router = new express.Router();

router.use('/auth', authRouter);
router.use('/checks', checkRouter);
router.use('/config', configRouter);
module.exports = router;
