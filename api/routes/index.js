const { Router } = require('express');
const { homeController } = require('../controllers/home');
const v1 = require('./v1');

const router = new Router();

router.get('/', homeController.index);
router.use('/v1', v1);

module.exports = router;
