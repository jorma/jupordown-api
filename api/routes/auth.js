const Router = require('express');
const passport = require('passport');
const { controller, validation } = require('../controllers/authentication');

const router = new Router();

router.post(
  '/register',
  validation.register,
  controller.register
);

router.post(
  '/login',
  validation.login,
  controller.login
);

router.get(
  '/me',
  validation.me,
  passport.authenticate('jwt', { session: false }),
  controller.me
);

router.get(
  '/status',
  passport.authenticate('jwt', { session: false }),
  controller.status
);

router.get(
  '/logout',
  passport.authenticate('jwt', { session: false }),
  controller.logout
);

router.get(
  '/logout/all',
  passport.authenticate('jwt', { session: false }),
  controller.logoutAll
);

module.exports = router;
