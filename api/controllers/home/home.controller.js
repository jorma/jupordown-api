const faker = require('Faker');
const packageJson = require('../../../package');

class HomeController {
  constructor() {
    this._name = faker.Internet.domainWord();
  }

  index(req, res, next) {
    const returnObj = {
      name: packageJson.name,
      version: packageJson.version,
      container: this._name
    };

    res.status(200).send(returnObj);
  }
}

const controller = new HomeController();

module.exports = {
  index: controller.index.bind(controller)
};
