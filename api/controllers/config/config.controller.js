const { getter } = require('../../services/config');

exports.get = async (req, res, next) => {
  try {
    const { name } = req.params;
    const config = await getter.getByName(name);

    if (!config) {
      return res.status(404).end();
    }

    res.status(200).send(config);
  } catch (error) {
    next(error);
  }
};
