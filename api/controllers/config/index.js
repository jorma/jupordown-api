const controller = require('./config.controller');
const validation = require('./config.validation');

module.exports = { controller, validation };
