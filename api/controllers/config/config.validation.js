const { celebrate, Joi } = require('celebrate');

const get = {
  params: {
    name: Joi.string()
  }
};

exports.get = celebrate(get);
