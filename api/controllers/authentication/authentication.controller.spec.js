const { getMockedExpressFns } = require('../../../utils/test/express');
const service = require('../../services/user');
const tokenService = require('../../services/token');
const controller = require('./authentication.controller');

jest.mock('../../services/user');
jest.mock('../../services/token');

beforeEach(() => {
  jest.resetAllMocks();
});

describe('Authentication Controller', () => {
  describe('register', () => {
    it('works as expected', async () => {
      service.create.mockResolvedValue({ _id: 'uuid-test' });

      const { req, res, next } = getMockedExpressFns({ body: { email: 'test@jest.com', password: '' } });
      const { register } = controller;

      await register(req, res, next);

      expect(next).not.toBeCalled();
      expect(res._statusMock).toHaveBeenCalledWith(200);
      expect(service.create).toBeCalledTimes(1);
      expect(service.create).toBeCalledWith({ email: 'test@jest.com', password: '' });
      expect(res._sendMock).toHaveBeenCalledWith({ user: { email: 'test@jest.com', id: 'uuid-test' } });
    });

    it('throws an error if something goes wrong', async () => {
      service.create.mockRejectedValue(new Error('JEST_MOCKED_ERROR'));
      const { req, res, next } = getMockedExpressFns({ body: { email: 'test@jest.com', password: '' } });
      const { register } = controller;

      await register(req, res, next);

      expect(next).toBeCalledWith(new Error('JEST_MOCKED_ERROR'));
      expect(res._statusMock).not.toHaveBeenCalled();
      expect(res._sendMock).not.toHaveBeenCalled();
    });
  });

  describe('login', () => {
    it('sends a 404 if the email not exists', async () => {
      const { login } = controller;
      const execSpy = jest.fn();

      service.model.findOne.mockReturnValue({ exec: execSpy.mockResolvedValue(undefined) });
      const { req, res, next } = getMockedExpressFns({ body: { email: 'test' } });
      await login(req, res, next);

      expect(service.model.findOne).toHaveBeenCalledWith({ email: 'test' });
      expect(next).not.toHaveBeenCalled();
      expect(res._statusMock).toHaveBeenCalledWith(404);
      expect(res._sendMock).toHaveBeenCalledWith(undefined);
    });

    it('if the password do not match send 404', async () => {
      const { login } = controller;
      const comparePasswordSpy = jest.fn().mockResolvedValue(false);
      const execSpy = jest.fn().mockResolvedValue({ comparePassword: comparePasswordSpy });
      service.model.findOne.mockReturnValue({ exec: execSpy });

      const { req, res, next } = getMockedExpressFns({ body: { email: 'test', password: 'TEST' } });
      await login(req, res, next);

      expect(service.model.findOne).toHaveBeenCalledWith({ email: 'test' });
      expect(comparePasswordSpy).toHaveBeenCalledWith('TEST');
      expect(next).not.toHaveBeenCalled();
      expect(res._statusMock).toHaveBeenCalledWith(404);
      expect(res._sendMock).toHaveBeenCalledWith(undefined);
    });

    it('if the email and password match sends 200 with token', async () => {
      const { login } = controller;
      const comparePasswordSpy = jest.fn().mockResolvedValue(true);
      const execSpy = jest.fn().mockResolvedValue({ comparePassword: comparePasswordSpy });
      service.model.findOne.mockReturnValue({ exec: execSpy });
      const date = new Date();
      tokenService.factory.create = jest.fn().mockResolvedValue({ jwt: 123, token: { expiry: date } });

      const { req, res, next } = getMockedExpressFns({ body: { email: 'test', password: 'TEST' } });
      await login(req, res, next);

      const expectedResponse = {
        token: '123',
        type: 'Bearer',
        until: date
      };

      expect(service.model.findOne).toHaveBeenCalledWith({ email: 'test' });
      expect(comparePasswordSpy).toHaveBeenCalledWith('TEST');
      expect(next).not.toHaveBeenCalled();
      expect(res._statusMock).toHaveBeenCalledWith(200);
      expect(res._sendMock).toHaveBeenCalledWith(expectedResponse);
    });

    it('if some exception occurs calls next', async () => {
      const { login } = controller;
      const comparePasswordSpy = jest.fn().mockRejectedValue(new Error('JEST_MOCKED_ERROR'));
      const execSpy = jest.fn().mockResolvedValue({ comparePassword: comparePasswordSpy });
      service.model.findOne.mockReturnValue({ exec: execSpy });

      const { req, res, next } = getMockedExpressFns({ body: { email: 'test', password: 'TEST' } });
      await login(req, res, next);

      expect(next).toBeCalledWith(new Error('JEST_MOCKED_ERROR'));
      expect(res._statusMock).not.toHaveBeenCalled();
      expect(res._sendMock).not.toHaveBeenCalled();
    });
  });

  describe('me', () => {
    it('returns a well formed DTO', async () => {
      const { me } = controller;
      const { req, res, next } = getMockedExpressFns({ user: { user: { _id: 'uuid-test', email: 'test@jest.com' } } });
      await me(req, res, next);

      expect(next).not.toHaveBeenCalled();
      expect(res._statusMock).toHaveBeenCalledWith(200);
      expect(res._sendMock).toHaveBeenCalledWith({ _id: 'uuid-test', email: 'test@jest.com' });
    });

    it('if fails excepts', async () => {
      const { me } = controller;
      const { req, res, next } = getMockedExpressFns({});
      await me(req, res, next);

      expect(next).toHaveBeenCalled();
      expect(res._statusMock).not.toHaveBeenCalled();
      expect(res._sendMock).not.toHaveBeenCalled();
    });
  });

  describe('status', () => {
    it('returns ok', async () => {
      const { status } = controller;
      const { req, res, next } = getMockedExpressFns({});
      await status(req, res, next);

      expect(next).not.toHaveBeenCalled();
      expect(res._statusMock).toHaveBeenCalledWith(200);
      expect(res._sendMock).toHaveBeenCalledWith({ status: 'ok' });
    });

    it('excepts and call next', async () => {
      const { status } = controller;

      const { req, res, next } = getMockedExpressFns({ body: { email: 'test', password: 'TEST' } });
      res._statusMock.mockImplementation(() => {
        throw new Error('JEST_MOCKED_ERROR');
      });

      await status(req, res, next);

      expect(next).toBeCalledWith(new Error('JEST_MOCKED_ERROR'));
      expect(res._sendMock).not.toHaveBeenCalled();
    });
  });

  describe('logout', () => {
    it('delete the token and returns ok', async () => {
      const { logout } = controller;
      tokenService.repository.delete = jest.fn().mockResolvedValue(undefined);
      const { req, res, next } = getMockedExpressFns({ user: { tokenId: 'tokenid' } });

      await logout(req, res, next);

      expect(tokenService.repository.delete).toBeCalledTimes(1);
      expect(tokenService.repository.delete).toBeCalledWith('tokenid');
      expect(res._statusMock).toBeCalledWith(200);
      expect(res._sendMock).toBeCalledWith({ status: 'ok' });
    });

    it('throws exception if some fails', async () => {
      const { logout } = controller;
      tokenService.repository.delete = jest.fn().mockRejectedValue(new Error('JEST_ERROR_MOCKED'));
      const { req, res, next } = getMockedExpressFns({ user: { tokenId: 'tokenid' } });

      await logout(req, res, next);

      expect(tokenService.repository.delete).toBeCalledTimes(1);
      expect(tokenService.repository.delete).toBeCalledWith('tokenid');
      expect(res._statusMock).not.toBeCalled();
      expect(next).toBeCalledWith(new Error('JEST_ERROR_MOCKED'));
    });
  });

  describe('logoutAll', () => {
    it('delete the tokens and returns ok', async () => {
      const { logoutAll } = controller;
      tokenService.repository.deleteByOwner = jest.fn().mockResolvedValue(undefined);
      const { req, res, next } = getMockedExpressFns({ user: { tokenId: 'tokenid', user: { _id: 123 } } });

      await logoutAll(req, res, next);

      expect(tokenService.repository.deleteByOwner).toBeCalledTimes(1);
      expect(tokenService.repository.deleteByOwner).toBeCalledWith(123);
      expect(res._statusMock).toBeCalledWith(200);
      expect(res._sendMock).toBeCalledWith({ status: 'ok' });
    });

    it('throws exception if some fails', async () => {
      const { logoutAll } = controller;
      tokenService.repository.deleteByOwner = jest.fn().mockRejectedValue(new Error('JEST_ERROR_MOCKED'));
      const { req, res, next } = getMockedExpressFns({ user: { tokenId: 'tokenid', user: { _id: 123 } } });

      await logoutAll(req, res, next);

      expect(tokenService.repository.deleteByOwner).toBeCalledTimes(1);
      expect(tokenService.repository.deleteByOwner).toBeCalledWith(123);
      expect(res._statusMock).not.toBeCalled();
      expect(next).toBeCalledWith(new Error('JEST_ERROR_MOCKED'));
    });
  });
});
