const { celebrate, Joi } = require('celebrate');

const register = {
  body: {
    email: Joi.string().email().required(),
    password: Joi.string().required()
  }
};

const login = {
  ...register,
  body: {
    ...register.body,
    forever: Joi.boolean().default(false)
  }
};

const me = {
  query: {}
};

exports.register = celebrate(register);
exports.login = celebrate(login);
exports.me = celebrate(me);
