const controller = require('./authentication.controller');
const validation = require('./authentication.validation');

module.exports = { controller, validation };
