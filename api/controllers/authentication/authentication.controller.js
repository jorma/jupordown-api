const userService = require('../../services/user');
const { factory: tokenFactory, repository: tokenRepository } = require('../../services/token');

const { model: User } = userService;
exports.register = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    const { _id } = await userService.create({ email, password });

    res.status(200).send({
      user: {
        email,
        id: _id
      }
    });
  } catch (error) {
    next(error);
  }
};

exports.login = async (req, res, next) => {
  try {
    const { email, forever, password } = req.body;
    const user = await User.findOne({ email }).exec();

    if (!user) {
      return res.status(404).send();
    }

    const match = await user.comparePassword(password);

    if (!match) {
      return res.status(404).send();
    }

    const { jwt, token } = await tokenFactory.create(user._id, { forever });
    res.status(200).send({
      token: `${jwt}`,
      type: 'Bearer',
      until: token.expiry
    });
  } catch (error) {
    next(error);
  }
};

exports.me = async (req, res, next) => {
  try {
    const { user } = req.user;

    res.status(200).send({
      _id: user._id,
      email: user.email
    });
  } catch (error) {
    next(error);
  }
};

exports.status = async (req, res, next) => {
  try {
    res.status(200).send({ status: 'ok' });
  } catch (error) {
    next(error);
  }
};

exports.logout = async (req, res, next) => {
  try {
    await tokenRepository.delete(req.user.tokenId);

    res.status(200).send({ status: 'ok' });
  } catch (error) {
    next(error);
  }
};

exports.logoutAll = async (req, res, next) => {
  try {
    await tokenRepository.deleteByOwner(req.user.user._id);

    res.status(200).send({ status: 'ok' });
  } catch (error) {
    next(error);
  }
};
