const { getMockedExpressFns } = require('../../../utils/test/express');
const validator = require('./authentication.validation');

beforeEach(() => {
  jest.resetAllMocks();
});

describe('Authentication Validator', () => {
  describe('register', () => {
    it('fails if receive no password', async () => {
      const { register } = validator;
      const { req, res, next } = getMockedExpressFns({
        body: {
          email: 'test@jest.com'
        },
        method: 'post'
      });

      await register(req, res, next);
      expect(next).not.toBeCalledWith();
      const [[call]] = next.mock.calls;
      expect(call.name).toBe('ValidationError');
      expect(call.details[0].message).toBe('"password" is required');
    });

    it('fails if receive no email', async () => {
      const { register } = validator;
      const { req, res, next } = getMockedExpressFns({
        body: {
          password: 'test'
        },
        method: 'post'
      });

      await register(req, res, next);
      expect(next).not.toBeCalledWith();
      const [[call]] = next.mock.calls;
      expect(call.name).toBe('ValidationError');
      expect(call.details[0].message).toBe('"email" is required');
    });

    it('fails if no receive email neither password', async () => {
      const { register } = validator;
      const { req, res, next } = getMockedExpressFns({
        body: {},
        method: 'post'
      });

      await register(req, res, next);
      expect(next).not.toBeCalledWith();
      const [[call]] = next.mock.calls;
      expect(call.name).toBe('ValidationError');
      expect(call.details[0].message).toBe('"email" is required');
    });

    it('register works as expected', async () => {
      const { register } = validator;
      const { req, res, next } = getMockedExpressFns({
        body: { email: 'test@jest.com', password: 'test' },
        method: 'post'
      });

      await register(req, res, next);
      expect(next).toBeCalledWith(null);
    });
  });

  describe('login', () => {
    it('fails if receive no password', async () => {
      const { login } = validator;
      const { req, res, next } = getMockedExpressFns({
        body: {
          email: 'test@jest.com'
        },
        method: 'post'
      });

      await login(req, res, next);
      expect(next).not.toBeCalledWith();
      const [[call]] = next.mock.calls;
      expect(call.name).toBe('ValidationError');
      expect(call.details[0].message).toBe('"password" is required');
    });

    it('fails if receive no email', async () => {
      const { login } = validator;
      const { req, res, next } = getMockedExpressFns({
        body: {
          password: 'test'
        },
        method: 'post'
      });

      await login(req, res, next);
      expect(next).not.toBeCalledWith();
      const [[call]] = next.mock.calls;
      expect(call.name).toBe('ValidationError');
      expect(call.details[0].message).toBe('"email" is required');
    });

    it('fails if no receive email neither password', async () => {
      const { login } = validator;
      const { req, res, next } = getMockedExpressFns({
        body: {},
        method: 'post'
      });

      await login(req, res, next);
      expect(next).not.toBeCalledWith();
      const [[call]] = next.mock.calls;
      expect(call.name).toBe('ValidationError');
      expect(call.details[0].message).toBe('"email" is required');
    });

    it('Works as expected', async () => {
      const { login } = validator;
      const { req, res, next } = getMockedExpressFns({
        body: { email: 'test@jest.com', password: 'test' },
        method: 'post',
        forever: true
      });

      await login(req, res, next);
      expect(next).toBeCalledWith(null);
    });
  });

  describe('me', () => {
    it('fails if anything is added as query param', async () => {
      const { me } = validator;
      const { req, res, next } = getMockedExpressFns({
        query: { test: '2' },
        method: 'get'
      });

      await me(req, res, next);
      expect(next).not.toBeCalledWith();
      const [[call]] = next.mock.calls;
      expect(call.name).toBe('ValidationError');
      expect(call.details[0].message).toBe('"test" is not allowed');
    });

    it('works without query params', async () => {
      const { me } = validator;
      const { req, res, next } = getMockedExpressFns({
        query: {},
        method: 'get'
      });

      await me(req, res, next);
      expect(next).toBeCalledWith(null);
    });
  });
});
