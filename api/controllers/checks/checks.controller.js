const { dispatcher } = require('../../services/dispatcher');
const { getter, Check } = require('../../services/check');

exports.list = async (req, res, next) => {
  try {
    const { user: { _id } } = req.user;
    const urls = await getter.getByUser(_id);

    res.status(200).send({ alias: urls.map(({ alias }) => alias) });
  } catch (error) {
    next(error);
  }
};

exports.create = async (req, res, next) => {
  try {
    const { url, cron, alias } = req.body;
    const { user } = req.user;
    await dispatcher.schedule({ url, cron, user, alias });

    res.status(200).end();
  } catch (error) {
    next(error);
  }
};

exports.update = async (req, res, next) => {
  try {
    const { alias } = req.params;
    const { cron } = req.body;
    const { user } = req.user;

    const check = await getter.findOne({ user: user._id, alias }, { _id: 0, url: 1 });
    if (!check) {
      return res.status(404).end();
    }

    const { url } = check;
    await dispatcher.reschedule({ url, cron, user, alias });

    res.status(200).end();
  } catch (error) {
    next(error);
  }
};

exports.remove = async (req, res, next) => {
  try {
    const { alias } = req.params;
    const { user: { _id: idUser } } = req.user;

    const check = await getter.findOne({ user: idUser, alias }, { _id: 0, url: 1 });
    if (!check) {
      return res.status(404).end();
    }

    const { url } = check;

    await Promise.all([
      dispatcher.cancel({ url, idUser }),
      Check.deleteMany({ user: idUser, alias })
    ]);

    res.status(200).send({ status: 'removed' });
  } catch (error) {
    next(error);
  }
};

exports.detail = async (req, res, next) => {
  try {
    const { pattern, start, end, group, status } = req.query;
    const { user: { _id: idUser } } = req.user;

    const [checks, stats] = await getter.get(idUser, { start, end, pattern, status, group });
    if (!checks || checks.length === 0) {
      return res.status(404).end();
    }

    const [{ url, alias }] = checks;
    return res.status(200).send({
      url,
      alias,
      stats,
      checks: checks.map(({ duration, date, ok, ko }) => ({ duration, date, ok, ko }))
    });
  } catch (error) {
    next(error);
  }
};
