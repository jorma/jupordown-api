const { celebrate, Joi } = require('celebrate');
const enums = require('../../../utils/enums');

const list = {
  query: Joi.object().keys({})
};

const create = {
  query: Joi.object().keys({}),
  body: Joi.object().keys({
    url: Joi.string().required(),
    alias: Joi.string().required(),
    cron: Joi.string().valid(enums.crons).required()
  })
};

const update = {
  query: Joi.object().keys({}),
  params: Joi.object().keys({
    alias: Joi.string().required()
  }),
  body: Joi.object().keys({
    cron: Joi.string().valid(enums.crons).required()
  })
};

const detail = {
  query: Joi.object().keys({
    pattern: Joi.string().required(),
    start: Joi.date().required(),
    end: Joi.date().required(),
    group: Joi.array().valid(enums.groups).default(enums.groups[0]),
    status: Joi.string().valid('ok', 'ko')
  })
};

const remove = {
  params: Joi.object().keys({
    alias: Joi.string().required()
  }),
  body: Joi.object().keys({})
};

exports.list = celebrate(list);
exports.create = celebrate(create);
exports.detail = celebrate(detail);
exports.remove = celebrate(remove);
exports.update = celebrate(update);
