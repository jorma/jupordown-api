const { getMockedExpressFns } = require('../../../utils/test/express');
const service = require('../../services/check');
const { dispatcher } = require('../../services/dispatcher');
const controller = require('./checks.controller');

beforeEach(() => {
  jest.resetAllMocks();
});

jest.mock('../../services/dispatcher');
jest.mock('../../services/check');

describe('Check Controller', () => {
  describe('list', () => {
    it('if some fails calls next with the error', async () => {
      const { list } = controller;
      const { req, res, next } = getMockedExpressFns({
        user: {
          user: {
            _id: 123
          }
        }
      });

      service.getter.getByUser.mockRejectedValue(new Error('JEST_ERROR_MOCKED'));
      await list(req, res, next);
      expect(next).toBeCalledWith(new Error('JEST_ERROR_MOCKED'));
    });

    it('if all works fine returns the list of checks', async () => {
      const { list } = controller;
      const { req, res, next } = getMockedExpressFns({
        user: {
          user: {
            _id: 123
          }
        }
      });

      service.getter.getByUser.mockResolvedValue([{ alias: 'alias_1' }]);

      await list(req, res, next);

      expect(next).not.toBeCalled();
      expect(res._statusMock).toBeCalledWith(200);
      expect(res._sendMock).toBeCalledWith({
        alias: ['alias_1']
      });
    });
  });

  describe('create', () => {
    it('if some fails calls next with the error', async () => {
      const { create } = controller;
      const { req, res, next } = getMockedExpressFns({
        body: {
          url: 'http://www.some-url.com',
          cron: '* * * * *',
          alias: 'some-alias'
        },
        user: {
          user: {
            _id: 123
          }
        }
      });

      dispatcher.schedule.mockRejectedValue(new Error('JEST_ERROR_MOCKED'));
      await create(req, res, next);
      expect(next).toBeCalledWith(new Error('JEST_ERROR_MOCKED'));
    });

    it('calls the scheduler and send response', async () => {
      const { create } = controller;
      const { req, res, next } = getMockedExpressFns({
        body: {
          url: 'http://www.some-url.com',
          cron: '* * * * *',
          alias: 'some-alias'
        },
        user: {
          user: {
            _id: 123
          }
        }
      });

      dispatcher.schedule.mockResolvedValue();

      await create(req, res, next);

      expect(dispatcher.schedule).toBeCalledTimes(1);
      expect(dispatcher.schedule).toBeCalledWith({
        url: 'http://www.some-url.com',
        cron: '* * * * *',
        alias: 'some-alias',
        user: { _id: 123 }
      });
      expect(res._statusMock).toBeCalledWith(200);
      expect(res._endMock).toBeCalled();
    });
  });

  describe('update', () => {
    it('if some fails calls next with the error', async () => {
      const { update } = controller;
      const { req, res, next } = getMockedExpressFns({
        user: {
          user: {
            _id: 123
          }
        },
        params: {
          alias: 'alias_1'
        },
        body: {
          cron: '* * * * *'
        }
      });

      dispatcher.reschedule.mockRejectedValue(new Error('JEST_ERROR_MOCKED'));
      service.getter.findOne.mockRejectedValue(new Error('JEST_ERROR_MOCKED'));
      await update(req, res, next);

      expect(next).toBeCalledWith(new Error('JEST_ERROR_MOCKED'));
    });

    it('if not check found returns 404', async () => {
      const { update } = controller;
      const { req, res, next } = getMockedExpressFns({
        user: {
          user: {
            _id: 123
          }
        },
        params: {
          alias: 'alias_1'
        },
        body: {
          cron: '* * * * *'
        }
      });

      dispatcher.reschedule.mockResolvedValue();
      service.getter.findOne.mockResolvedValue();

      await update(req, res, next);

      expect(dispatcher.reschedule).not.toBeCalled();
      expect(service.getter.findOne).toBeCalledTimes(1);
      expect(res._statusMock).toBeCalledWith(404);
      expect(res._endMock).toBeCalled();
    });

    it('if check found returns 200 and reschedule', async () => {
      const { update } = controller;
      const { req, res, next } = getMockedExpressFns({
        user: {
          user: {
            _id: 123
          }
        },
        params: {
          alias: 'alias_1'
        },
        body: {
          cron: '* * * * *'
        }
      });

      dispatcher.reschedule.mockResolvedValue();
      service.getter.findOne.mockResolvedValue({ url: 'https://some-url.com' });

      await update(req, res, next);

      expect(dispatcher.reschedule).toBeCalledWith({ url: 'https://some-url.com', cron: req.body.cron, user: req.user.user, alias: req.params.alias });
      expect(dispatcher.reschedule).toBeCalledTimes(1);
      expect(service.getter.findOne).toBeCalledTimes(1);
      expect(res._statusMock).toBeCalledWith(200);
      expect(res._endMock).toBeCalled();
    });
  });
});
