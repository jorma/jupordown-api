const controller = require('./checks.controller');
const validation = require('./checks.validation');

module.exports = { controller, validation };
