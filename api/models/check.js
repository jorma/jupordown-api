const mongoose = require('mongoose');

const { Schema } = mongoose;
const CheckSchema = new Schema({
  url: String,
  duration: Number,
  status: {
    type: String,
    enum: ['ok', 'ko']
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  alias: String
}, { timestamps: true });

CheckSchema.index({ user: 1, url: 1, createdAt: 1 });
CheckSchema.index({ user: 1, alias: 1, createdAt: 1 });

module.exports = mongoose.model('Check', CheckSchema);
