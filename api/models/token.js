const mongoose = require('mongoose');

const { Schema } = mongoose;
const TokenSchema = new Schema({
  token: String,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  expiry: Date
}, { timestamps: true });

TokenSchema.index({ expiry: 1 }, { expireAfterSeconds: 0 });

module.exports = mongoose.model('Token', TokenSchema);
