const Check = require('./check');

describe('Check Model', () => {
  describe('fields', () => {
    it('no more than necessary fields', async () => {
      const { obj: schema } = Check.schema;

      expect(schema).toHaveProperty('url');
      expect(schema).toHaveProperty('duration');
      expect(schema).toHaveProperty('status');
      expect(schema).toHaveProperty('user');
      expect(schema).toHaveProperty('alias');
    });
  });

  describe('schema', () => {
    it('have correct options', async () => {
      const { options } = Check.schema;

      expect(options).toHaveProperty('timestamps', true);
    });
  });
});
