const Token = require('./token');

describe('Token Model', () => {
  describe('fields', () => {
    it('no more than necessary fields', async () => {
      const { obj: schema } = Token.schema;

      expect(schema).toHaveProperty('token');
      expect(schema).toHaveProperty('user');
      expect(schema).toHaveProperty('expiry');
    });
  });

  describe('schema', () => {
    it('have correct options', async () => {
      const { options } = Token.schema;

      expect(options).toHaveProperty('timestamps', true);
    });
  });
});
