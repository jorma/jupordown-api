const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const { Schema } = mongoose;
const UserSchema = new Schema({
  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: true
  },
  name: {
    type: String
  },
  password: {
    type: String,
    required: true
  },
  salt: {
    type: String
  }
}, { timestamps: true });

async function preSaveHook(next) {
  try {
    const user = this;
    if (user.isModified('password') || user.isNew) {
      const salt = await bcrypt.genSalt(10);
      const hash = await bcrypt.hash(user.password, salt);

      user.salt = salt;
      user.password = hash;
    }

    return next();
  } catch (error) {
    return next(error);
  }
}

UserSchema.pre('save', preSaveHook);

UserSchema.methods.comparePassword = async function (pw) {
  try {
    const user = this;
    const isMatch = await bcrypt.compare(pw, user.password);

    return isMatch;
  } catch (error) {
    throw error;
  }
};

module.exports = mongoose.model('User', UserSchema);
module.exports._middlewares = { preSaveHook };
