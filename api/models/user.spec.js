const bcrypt = require('bcrypt');
const User = require('./user');

function getUser(options = {}) {
  const user = new User({ name: 'Test', password: 'test' });
  user.isNew = Boolean(options.isNew);
  jest.spyOn(user, 'isModified').mockImplementation((_) => options.isModified);

  return user;
}

jest.mock('bcrypt');

beforeEach(() => {
  jest.resetAllMocks();
});

describe('User Model', () => {
  describe('fields', () => {
    it('no more than necessary fields', () => {
      const { obj: schema } = User.schema;

      expect(schema).toHaveProperty('email');
      expect(schema).toHaveProperty('name');
      expect(schema).toHaveProperty('password');
      expect(schema).toHaveProperty('salt');
    });
  });

  describe('schema', () => {
    it('correct schema options', () => {
      const { options } = User.schema;
      expect(options).toHaveProperty('timestamps', true);
    });
  });

  describe('hooks', () => {
    it('presave do not generate salt and password if the user is not modified', async (done) => {
      const user = getUser({ isNew: false, isModified: false });

      const middlewareFn = User._middlewares.preSaveHook.bind(user);
      const next = () => {
        expect(bcrypt.genSalt).not.toBeCalled();
        expect(user.password).toBe('test');
        expect(user.salt).toBeUndefined();

        done();
      };

      middlewareFn(next);
    });

    it('presave hook generates new password and salt if necessary', async (done) => {
      const user = getUser({ isNew: true, isModified: true });
      const middlewareFn = User._middlewares.preSaveHook.bind(user);
      bcrypt.genSalt.mockResolvedValue('my_salt');
      bcrypt.hash.mockResolvedValue('my_hash');

      const next = () => {
        expect(bcrypt.genSalt).toBeCalled();
        expect(bcrypt.hash).toBeCalled();
        expect(user.password).toBeDefined();
        expect(user.salt).toBeDefined();
        expect(user.password).toBe('my_hash');

        done();
      };

      middlewareFn(next);
    });

    it('presave hook throws error if some fails', (done) => {
      const user = getUser({ isNew: true, isModified: true });
      const middlewareFn = User._middlewares.preSaveHook.bind(user);
      bcrypt.genSalt.mockRejectedValue(new Error('JEST_ERROR_MOCKED'));
      bcrypt.hash.mockResolvedValue('my_hash');

      const next = (error) => {
        expect(bcrypt.genSalt).toBeCalledTimes(1);
        expect(bcrypt.hash).not.toBeCalledTimes(1);
        expect(error).toBeDefined();
        expect(error.message).toBe('JEST_ERROR_MOCKED');

        done();
      };

      middlewareFn(next);
    });
  });

  describe('model functions', () => {
    it('compare password returns true if is the same password', (done) => {
      const { compare, genSalt, hash } = jest.requireActual('bcrypt');
      bcrypt.compare.mockImplementation(compare);
      bcrypt.genSalt.mockImplementation(genSalt);
      bcrypt.hash.mockImplementation(hash);

      const user = getUser({ isNew: true, isModified: true });
      const middlewareFn = User._middlewares.preSaveHook.bind(user);

      const next = async () => {
        const match = await user.comparePassword('test');

        expect(bcrypt.compare).toBeCalledTimes(1);
        expect(bcrypt.compare).toBeCalledWith('test', user.password);
        expect(match).toBeTruthy();

        done();
      };

      middlewareFn(next);
    });

    it('compare password returns false if is not the same password', (done) => {
      const { compare, genSalt, hash } = jest.requireActual('bcrypt');
      bcrypt.compare.mockImplementation(compare);
      bcrypt.genSalt.mockImplementation(genSalt);
      bcrypt.hash.mockImplementation(hash);

      const user = getUser({ isNew: true, isModified: true });
      const middlewareFn = User._middlewares.preSaveHook.bind(user);

      const next = async () => {
        const match = await user.comparePassword('TEST');

        expect(bcrypt.compare).toBeCalledTimes(1);
        expect(bcrypt.compare).toBeCalledWith('TEST', user.password);
        expect(match).toBeFalsy();

        done();
      };

      middlewareFn(next);
    });

    it('compare password throws an error if something fails', (done) => {
      const { genSalt, hash } = jest.requireActual('bcrypt');
      bcrypt.compare.mockRejectedValue(new Error('JEST_ERROR_MOCKED'));
      bcrypt.genSalt.mockImplementation(genSalt);
      bcrypt.hash.mockImplementation(hash);

      const user = getUser({ isNew: true, isModified: true });
      const middlewareFn = User._middlewares.preSaveHook.bind(user);

      const next = async () => {
        try {
          await user.comparePassword('TEST');
          expect(true).toBeFalsy();

          done();
        } catch (error) {
          expect(error.message).toBe('JEST_ERROR_MOCKED');

          done();
        }
      };

      middlewareFn(next);
    });
  });
});
