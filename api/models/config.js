const mongoose = require('mongoose');

const { Schema } = mongoose;
const ConfigSchema = new Schema({
  name: String,
  color: String
}, { timestamps: true });

ConfigSchema.index({ name: 1 });

module.exports = mongoose.model('Config', ConfigSchema);
