const Config = require('../../models/config');

class Getter {
  async getByName(name) {
    return Config.findOne({ name }, { _id: 0, name: 1, color: 1 }).lean().exec();
  }
}

module.exports = new Getter();
