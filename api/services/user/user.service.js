const User = require('../../models/user');

exports.create = async ({ email, password }) => {
  const user = new User({ email, password });
  const { _id } = await user.save();

  user._id = _id;

  return user;
};
