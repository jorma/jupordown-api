const Check = require('../../models/check');
const getter = require('./getter.class');
const service = require('./check.service');

module.exports = { getter, Check, service };
