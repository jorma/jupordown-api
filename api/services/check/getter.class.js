const Check = require('../../models/check');
const TIME_MAP = require('../../../utils/maps/intervals.map');

class Getter {
  async getByUser(idUser) {
    const pipeline = [{
      $match: { user: idUser }
    }, {
      $group: {
        _id: { alias: '$alias' },
        alias: { $first: '$alias' }
      }
    }, {
      $project: {
        _id: 0,
        alias: 1
      }
    }];

    return Check.aggregate(pipeline);
  }

  async findOne(query, projection) {
    return Check.findOne(query, projection).lean().exec();
  }

  async get(idUser, opts = {}) {
    const { start, end, pattern, status, group } = opts;
    const statuses = status ? [status] : ['ok', 'ko'];

    const pipeline = [{
      $match: {
        $and: [
          { user: idUser },
          {
            $or: [
              { url: pattern },
              { alias: pattern }
            ]
          },
          { createdAt: { $gte: start } },
          { createdAt: { $lte: end } },
          { status: { $in: statuses } }
        ]
      }
    }, {
      $facet: {
        checks: [{
          $group: {
            _id: {
              $toDate: {
                $subtract: [
                  { $toLong: '$createdAt' },
                  { $mod: [{ $toLong: '$createdAt' }, 1000 * 60 * TIME_MAP.get(group)] }
                ]
              }
            },
            ok: {
              $sum: {
                $cond: {
                  if: {
                    $eq: ['$status', 'ok']
                  },
                  then: 1,
                  else: 0
                }
              }
            },
            ko: {
              $sum: {
                $cond: {
                  if: {
                    $eq: ['$status', 'ko']
                  },
                  then: 1,
                  else: 0
                }
              }
            },
            alias: { $first: '$alias' },
            url: { $first: '$url' },
            duration: { $avg: '$duration' },
            date: { $first: '$createdAt' }
          }
        }, {
          $sort: {
            date: 1
          }
        }, {
          $project: {
            _id: 0,
            alias: 1,
            url: 1,
            status: 1,
            duration: 1,
            date: 1,
            ok: 1,
            ko: 1
          }
        }],
        stats: [{
          $group: {
            _id: { alias: '$alias' },
            ok: {
              $sum: {
                $cond: {
                  if: {
                    $eq: ['$status', 'ok']
                  },
                  then: 1,
                  else: 0
                }
              }
            },
            ko: {
              $sum: {
                $cond: {
                  if: {
                    $eq: ['$status', 'ko']
                  },
                  then: 1,
                  else: 0
                }
              }
            },
            avg: { $avg: '$duration' },
            min: { $min: '$duration' },
            max: { $max: '$duration' }
          }
        }, {
          $project: {
            _id: 0
          }
        }]
      }
    }];

    const [{ checks, stats: [stats] }] = await Check.aggregate(pipeline);

    return [checks, stats];
  }
}

module.exports = new Getter();
