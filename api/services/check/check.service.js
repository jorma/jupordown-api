const dates = require('../../../utils/dates');
const arrays = require('../../../lib/arrays');

class CheckService {
  _transform(check) {
    return {
      duration: [check.duration],
      date: check.createdAt,
      ok: Number(check.status === 'ok'),
      ko: Number(check.status === 'ko')
    };
  }

  _hit(last, check) {
    last.duration.push(check.duration);
    last.ok += Number(check.status === 'ok');
    last.ko += Number(check.status === 'ko');
  }

  zip(checks, granularity) {
    const zipped = checks.reduce((zip, check, index) => {
      if (index === 0) {
        zip.push(this._transform(check));

        return zip;
      }

      const last = zip[zip.length - 1];
      const difference = dates.diff(check.createdAt, last.date, granularity);

      if (difference === 0 && granularity !== 'none') {
        this._hit(last, check);
        zip[zip.length - 1] = last;

        return zip;
      }

      zip.push(this._transform(check));

      return zip;
    }, []).map((z) => {
      const { duration } = z;
      z.duration = arrays.sum(duration) / duration.length;

      return z;
    });

    return zipped;
  }
}

module.exports = new CheckService();
