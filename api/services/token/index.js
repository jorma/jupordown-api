const factory = require('./token.factory');
const repository = require('./token.repository');

module.exports = { factory, repository };
