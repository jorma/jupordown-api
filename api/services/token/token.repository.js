const { Types } = require('mongoose');
const Repository = require('../../../utils/repository.class');
const Token = require('../../models/token');

class TokenRepository extends Repository {
  async delete(idToken) {
    const deleted = await this._model.remove({ _id: idToken }).exec();

    return deleted;
  }

  async deleteByOwner(user) {
    const deleted = await this._model.remove({ user: new Types.ObjectId(user) }).exec();

    return deleted;
  }

  async getOwner(idToken) {
    const pipeline = [{
      $match: {
        _id: new Types.ObjectId(idToken)
      }
    }, {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'user'
      }
    }, {
      $project: {
        _id: 0,
        user: 1
      }
    }, {
      $unwind: '$user'
    }, {
      $replaceRoot: {
        newRoot: '$user'
      }
    }, {
      $project: {
        __v: 0
      }
    }];

    const [owner] = await this._model.aggregate(pipeline);

    return owner;
  }
}

module.exports = new TokenRepository(Token);
