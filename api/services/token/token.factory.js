const jwt = require('jsonwebtoken');
const dayjs = require('dayjs');
const Token = require('../../models/token');
const config = require('../../../config/environment');

async function storeToken(tokenModel) {
  const { _id } = await tokenModel.save();

  return { _id };
}

exports.create = async (user, options = {}) => {
  const obj = {
    user,
    expiry: dayjs(new Date()).add(options.forever ? 365 : 2, 'days').toDate()
  };

  const token = new Token(obj);
  const session = await storeToken(token);
  const jwtToken = jwt.sign(session, config.auth.secret, { expiresIn: options.forever ? '365 days' : '2 days' });

  return { jwt: jwtToken, token };
};
