const Agenda = require('agenda');
const logger = require('../../../utils/logger');
const check = require('./jobs/check');

class Dispatcher {
  constructor() {
    this.agenda = new Agenda();
  }

  _addListeners() {
    const listeners = require('./jobs/handlers');

    this.agenda.on('start', listeners.start);
    this.agenda.on('complete', listeners.complete);
    this.agenda.on('success', listeners.success);
    this.agenda.on('fail', listeners.fail);
    this.agenda.on('error', listeners.error);
  }

  _configureAgenda() {
    this.agenda.processEvery('5 seconds');
    this.agenda.maxConcurrency(20);
    this.agenda.defaultConcurrency(5);
    this.agenda.lockLimit(0);
    this.agenda.defaultLockLimit(0);
    this.agenda.defaultLockLifetime(5000);
    this.agenda.sort({ nextRunAt: 1, priority: -1 });
  }

  async init(name, mongoConf) {
    return new Promise((resolve, reject) => {
      const { uri, col } = mongoConf;
      this._configureAgenda();
      this.agenda.name(name);
      this.agenda.database(uri, col, { useNewUrlParser: true });
      this.agenda.once('error', () => {
        logger.error('MongoDB Server is down!');
        reject();
      });

      this.agenda.once('ready', () => {
        logger.info('Connected to MongoDB');
        resolve();
      });

      this._addListeners();
    });
  }

  getAgenda() {
    return this.agenda;
  }

  async start() {
    const jobs = await this.agenda.jobs({ name: /check/ });
    jobs.forEach((job) => {
      const { name } = job.attrs;
      this.agenda.define(name, check);
    });

    this.agenda.start();
  }

  async cancel(params, options = {}) {
    const { url, idUser } = params;

    const name = new RegExp(`^check_${idUser}_${url}`, 'i');
    const jobs = await this.agenda.jobs({ name });
    const ps = jobs.map(async (job) => job.remove());
    const resolved = await Promise.all(ps);

    return resolved.every((r) => r === 1);
  }

  async schedule(params, options = {}) {
    const { cron, url, user, alias } = params;
    const name = `check_${user._id}_${url}_${cron.replace(' ', '_')}`;

    this.agenda.define(name, check);
    this.agenda.every(cron, name, { url, user, alias });
  }

  async reschedule(params, options = {}) {
    const { url, user, cron, alias } = params;

    await this.cancel({ url, idUser: user._id });
    await this.schedule({ url, cron, user, alias });
  }
}

module.exports = new Dispatcher();
