const logger = require('../../../../utils/logger');

exports.start = (job) => {
  logger.debug(`Job ${job.attrs.name} starting`);
};

exports.complete = (job) => {
  logger.debug(`Job ${job.attrs.name} finished`);
};

exports.success = async (job) => {
  logger.debug(`Job ${job.attrs.name} success`);
};

exports.fail = async (error, job) => {
  logger.debug(`Job ${job.attrs.name} failed`);
};

exports.error = (error) => {
  throw error;
};
