const axios = require('axios');
const hrtime = require('process.hrtime');
const Check = require('../../../models/check');

axios.interceptors.request.use((config) => {
  config.metadata = { startTime: hrtime() };

  return config;
}, (error) => {
  return Promise.reject(error);
});

axios.interceptors.response.use((response) => {
  const { startTime } = response.config.metadata;
  response.config.metadata.endTime = Math.round(hrtime(startTime, 'ms'));

  return response;
}, (error) => {
  const { startTime } = error.config.metadata;
  error.config.metadata.endTime = Math.round(hrtime(startTime, 'ms'));

  return Promise.reject(error);
});

async function check(job, done) {
  try {
    const { url, user, alias } = job.attrs.data;
    let errored = false;
    let duration = null;
    try {
      const response = await axios.get(url);
      duration = response.config.metadata.endTime;
    } catch (error) {
      duration = error.config.metadata.endTime;
      errored = true;
    }

    const check = new Check({
      url,
      duration,
      user: user._id,
      status: errored ? 'ko' : 'ok',
      alias
    });
    await check.save();

    done();
  } catch (error) {
    done(error);
  }
}

module.exports = check;
