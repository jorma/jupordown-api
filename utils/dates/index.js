const dayjs = require('dayjs');

function diff(date1, date2, granularity) {
  const dayjs1 = dayjs(date1);
  const dayjs2 = dayjs(date2);

  const difference = dayjs1.diff(dayjs2, granularity);

  return difference;
}

module.exports = {
  diff
};
