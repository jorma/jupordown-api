module.exports = new Map([
  ['minute', 1],
  ['hour', 60],
  ['day', 60 * 24],
  ['week', 60 * 24 * 7]
]);
