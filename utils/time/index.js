function beautyHr(hr) {
  return `Execution time (hr): ${hr[0]}s ${hr[1] / 1000000}ms`;
}

module.exports = {
  beautyHr
};
