const crons = [
  '15 seconds',
  '30 seconds',
  '1 minutes',
  '5 minutes',
  '15 minutes',
  '30 minutes',
  '1 hours',
  '6 hours',
  '12 hours',
  '1 days'
];

const groups = [
  'minute',
  'hour',
  'day'
];

module.exports = { crons, groups };
