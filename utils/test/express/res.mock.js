class ResponseMock {
  constructor() {
    this._statusMock = jest.fn((n) => this);
    this._sendMock = jest.fn();
    this._endMock = jest.fn();
    this._jsonMock = jest.fn();
  }

  status(n) {
    return this._statusMock(n);
  }

  send(obj) {
    return this._sendMock(obj);
  }

  end() {
    return this._endMock();
  }

  json(obj) {
    return this._jsonMock(obj);
  }
}

module.exports = ResponseMock;
