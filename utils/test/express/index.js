const ResponseMock = require('./res.mock');

function getMockedExpressFns(req) {
  return {
    req,
    res: new ResponseMock(),
    next: jest.fn()
  };
}

module.exports = {
  getMockedExpressFns
};
