const rTracer = require('cls-rtracer');
const winston = require('winston');
const config = require('../../config/environment');

class Logger {
  constructor(logLevel) {
    this.level = logLevel;
    this.logger = winston.createLogger({
      transports: this.transports
    });
  }

  get transports() {
    const transports = [];

    if (config.env !== 'test') {
      transports.push(this.console);
    }

    return transports;
  }

  get console() {
    const { format: { colorize, timestamp, align, printf, combine } } = winston;

    return new winston.transports.Console({
      level: this.level,
      format: combine(
        colorize(),
        timestamp(),
        align(),
        printf((info) => {
          const rid = rTracer.id();

          return `${info.timestamp} ${info.level}${rid ? ` [request-id:${rid}]` : ''}: ${info.message.trim()}`;
        })
      )
    });
  }

  get stream() {
    return {
      write: (message, encoding) => {
        this.logger.info(message);
      }
    };
  }
}

module.exports = new Logger(config.logger.level);
