require('dotenv').config();
const path = require('path');
const { merge } = require('lodash');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.TZ = 'utc';

const env = process.env.NODE_ENV;
const tz = process.env.TZ;
const envConfig = require(`./${env}.js`);

const rootPath = path.normalize(`${__dirname}/../..`);

const all = {
  env,
  tz,
  root: rootPath,
  port: 9000,
  logger: {
    level: 'debug'
  },
  mongo: {
    uri: 'mongodb://localhost:27017/jupordown',
    options: {
      useCreateIndex: true,
      useNewUrlParser: true
    }
  },
  auth: {
    secret: 'topsecret'
  }
};

const processEnv = {
  port: process.env.PORT,
  logger: {
    level: process.env.LOG_LEVEL
  },
  mongo: {
    uri: process.env.MONGO_URI
  },
  auth: {
    secret: process.env.AUTH_SECRET
  }
};

const config = merge(all, envConfig, processEnv);

module.exports = config;
