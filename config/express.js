const rTracer = require('cls-rtracer');
const express = require('express');
const bodyParser = require('body-parser');
const celebrate = require('celebrate');
const morgan = require('morgan');
const passport = require('passport');
const index = require('../api/routes/index'); // eslint-disable-line unicorn/import-index
const logger = require('../utils/logger');
const accessControl = require('./middlewares/access-control');
const handler = require('./middlewares/error-handler');

const app = express();
app.enable('trust proxy');
app.use(rTracer.expressMiddleware());

app.use(morgan('tiny', { stream: logger.stream }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
require('./passport')(passport);

app.use(accessControl);
app.use('/', index);

app.use(celebrate.errors());
app.use(handler);

module.exports = app;
