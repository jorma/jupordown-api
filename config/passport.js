const { Strategy, ExtractJwt } = require('passport-jwt');
const { repository: tokenRepository } = require('../api/services/token');
const config = require('./environment');

module.exports = function (passport) {
  const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.auth.secret,
    passReqToCallback: true
  };

  passport.use(new Strategy(opts, async (req, jwtPayload, done) => {
    try {
      const user = await tokenRepository.getOwner(jwtPayload._id);
      if (!user) {
        return done(null, false);
      }

      return done(null, { user, tokenId: jwtPayload._id });
    } catch (error) {
      done(error, false);
    }
  }));
};
