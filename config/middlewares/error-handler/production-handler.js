const logger = require('../../../utils/logger');

function errorParser(err) {
  if (err.stack) {
    return err.stack.toString();
  }

  return err.toString();
}

function sendResponse(err, req, res, next) {
  if (res.headersSent) {
    return next(err);
  }

  if (err.statusCode) {
    res.statusCode = err.statusCode;
  }

  if (err.status) {
    res.statusCode = err.status;
  }

  if (res.statusCode < 400) {
    res.statusCode = 500;
  }

  return res.send();
}

module.exports = (err, req, res, next) => {
  let errorString = errorParser(err);

  errorString = errorString.split('\n');
  errorString = errorString.splice(0, 10);

  logger.error(errorString.join('\n'));

  return sendResponse(err, req, res, next);
};
