const devHandler = require('errorhandler');
const config = require('../../environment');
const logger = require('../../../utils/logger');
const productionHandler = require('./production-handler');

const handler = config.env === 'production' ? productionHandler : devHandler({ log: (error, str) => logger.error(str) });

module.exports = handler;
