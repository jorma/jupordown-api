function sum(array) {
  return array.reduce((total, current) => total + current, 0);
}

module.exports = {
  sum
};
