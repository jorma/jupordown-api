const http = require('http');
const config = require('./config/environment');
const app = require('./config/express');
const logger = require('./utils/logger');
const db = require('./utils/db');
const { dispatcher } = require('./api/services/dispatcher');

async function initialize() {
  try {
    const server = http.createServer(app);
    const dispatcherConnectPromise = dispatcher.init('jupordown-queue', { uri: config.mongo.uri, col: 'agenda-checks' });
    const mongoConnectPromise = db.init(config.mongo.uri, config.mongo.options);
    await Promise.all([dispatcherConnectPromise, mongoConnectPromise]);
    dispatcher.start();

    logger.info('Successfully connected to MongoDB Server');
    server.listen(config.port, () => {
      logger.info(`Server listening on ${config.port}, in ${config.env} mode`);
    });
  } catch (error) {
    logger.error('Something went wrong!');
    logger.error(error.message);
    await initialize();
  }
}

initialize();
